/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:seaton-preference
 * @Title:SeatonPreference
 * @Short_Description: libseaton is a library that provides APIs to store and retrieve persistent data for services and applications.
 *
 *
 * libseaton is a library to store and retrieve persistent data. It is used by services as well as apps. The libseaton uses SQLite3 to store the persistent data. It however provides simple APIs instead of complex queries to get or set the data.
 * The apps have to explicitly call the APIs to store the data it intends to save. The App FW will not make these calls explicitly.
 * The database for apps and for services will be stored under the variable
 * folder for that bundle ID (see https://wiki.apertis.org/Application_Layout).
 */

#include "seaton-preference.h"
#include <glib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <semaphore.h>
#include <errno.h>
#include <stdlib.h>

struct _SeatonPreferencePrivate
{
  sqlite3  *pDbHandle;
  int   	  inonBlocking;
  int   	  iuseSemaphores;
  sem_t 	 *psemid;	/* not used right now */
};

G_DEFINE_TYPE_WITH_PRIVATE (SeatonPreference, seaton_preference, G_TYPE_OBJECT)

#define LIBSEATON_pm_errhandler(stmt,args...) \
   fprintf(stderr, stmt ". Error occurred in file %s line %d\n", ##args, __FILE__, __LINE__)

static gchar *replace(const gchar *pSource, const gchar *pOld, const gchar *pNew);

#define PREFERENCE_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), SEATON_TYPE_PREFERENCE, SeatonPreferencePrivate))

static void
seaton_preference_get_property (GObject    *object,
                                guint       property_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
seaton_preference_set_property (GObject      *object,
                                guint         property_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
seaton_preference_dispose (GObject *object)
{
  G_OBJECT_CLASS (seaton_preference_parent_class)->dispose (object);
}

static void
seaton_preference_finalize (GObject *object)
{
  G_OBJECT_CLASS (seaton_preference_parent_class)->finalize (object);
}

static void
seaton_preference_class_init (SeatonPreferenceClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = seaton_preference_get_property;
  object_class->set_property = seaton_preference_set_property;
  object_class->dispose = seaton_preference_dispose;
  object_class->finalize = seaton_preference_finalize;
}

static void
seaton_preference_init (SeatonPreference *self)
{
  self->priv = PREFERENCE_PRIVATE (self);
}

/**
 * seaton_preference_new:
 *
 * Constructor.Use this API to create a new #SeatonPreference object
 *
 * Returns: #SeatonPreference object
 */
SeatonPreference *
seaton_preference_new (void)
{
  return g_object_new (SEATON_TYPE_PREFERENCE, NULL);
}

/**
 * seaton_preference_remove:
 * @pGobj: The pdi object Parameter
 * @pTableName:Column name
 * @pKeyStr: The  key Name
 * @pCondStr: The  Condition value or the Key
 *
 * This API will delete the persistent data identified by the "key" from the PDI database. *
 * The calling thread is blocked until a reply is received.
 *
 * Returns: the error/success value
 */
gint
seaton_preference_remove (SeatonPreference *pGobj,
                          gchar *pTableName,
                          const gchar* pKeyStr,
                          gchar *pCondStr)
{
	gint 		inResult;
	gchar 		*chSqlStr;
	gchar 		*pzErrMsg = NULL;

	SeatonPreferencePrivate *priv = pGobj->priv;
	if(pGobj == NULL)
	{
	  LIBSEATON_pm_errhandler("ERROR: PDI is NULL");
	  return SEATON_PREFERENCE_PM_DBE;
	}

	gchar *pTmpTableName = NULL;
	if(SEATON_PREFERENCE_PM_OK);
  if(pTableName != NULL)
  {
    pTmpTableName = strdup(pTableName);
  }
  else
  {
    pTmpTableName = strdup("PropertyName");
  }

	if(pKeyStr == NULL && pCondStr == NULL)
	{
	  chSqlStr = g_strdup_printf("DELETE FROM %s;",pTmpTableName);
	}
	else
	{
    chSqlStr = g_strdup_printf("DELETE FROM %s WHERE %s='%s';",pTmpTableName, pKeyStr, pCondStr);
	}

	inResult = sqlite3_exec(priv->pDbHandle, chSqlStr, 0, 0, &pzErrMsg);

	g_free(chSqlStr);
	g_free(pTmpTableName);

	if(inResult != SQLITE_OK)
  {
		if(NULL != pzErrMsg )
		{
			LIBSEATON_pm_errhandler("SQL Error: %s", pzErrMsg);
			sqlite3_free(pzErrMsg);
		}
    return SEATON_PREFERENCE_PM_UE;
  }

	return SEATON_PREFERENCE_PM_OK;//success
}

/**
 * seaton_preference_set:
 * @pGobj: The object Parameter
 * @pTableName: column name
 * @pUniqueKey: The Unique Key
 * @pUniqueValue: The unique value for the key
 * @pKeyStr: The  key Name
 * @pData: The Data to be updated
 *
 * API to update preference database given the key based on condition.
 * The calling thread is blocked until a reply is received.
 *
 * Returns: the error/success value
 */
gint
seaton_preference_set (SeatonPreference *pGobj,
                       gchar *pTableName,
                       const gchar *pUniqueKey,
                       gchar *pUniqueValue,
                       const gchar* pKeyStr,
                       gchar *pData)
{
	gchar			*chSqlStr = NULL;
	gint 			inResult;
	gchar 		*pzErrMsg = NULL ;

	SeatonPreferencePrivate *priv = pGobj->priv;
	if(pGobj == NULL)
	{
	   LIBSEATON_pm_errhandler("ERROR: PDI is NULL");
	   return SEATON_PREFERENCE_PM_DBE;
	}

	gchar *pTmpTableName = NULL;
  if(pTableName != NULL)
  {
    pTmpTableName = strdup(pTableName);
  }
  else
  {
     pTmpTableName = strdup("PropertyName");
  }

	if(pUniqueKey != NULL && pUniqueValue != NULL )
	{
	  chSqlStr = g_strdup_printf("UPDATE %s SET '%s'='%s' WHERE %s='%s';",pTmpTableName,pKeyStr, pData, pUniqueKey, pUniqueValue);
	}
	else if(pUniqueKey == NULL && pUniqueValue == NULL )
	{
	  chSqlStr = g_strdup_printf("UPDATE %s SET '%s'='%s';",pTmpTableName,pKeyStr, pData);
	}

	g_assert(chSqlStr != NULL);
	inResult = sqlite3_exec(priv->pDbHandle, chSqlStr, 0, 0, &pzErrMsg);
	if(inResult != SQLITE_OK)
	{
		if(NULL != pzErrMsg)
		{
			LIBSEATON_pm_errhandler("SQL Error: %s", pzErrMsg);
			sqlite3_free(pzErrMsg);
		}
     return SEATON_PREFERENCE_PM_UE;
	}

	g_free(pTmpTableName);
	g_free(chSqlStr);

	return SEATON_PREFERENCE_PM_OK;//success

}

/**
 * seaton_preference_set_multiple:
 * @pGobj: The object Parameter
 * @pTableName: Table name
 * @hash: hash table of keys with values to be updated
 * @pConditon: the condition for the preference
 *
 * will update multiple rows (keys) in the database depending upon the conditions *
 * The calling thread is blocked until a reply is received.
 *
 * Returns: the error/success value
 */
gint
seaton_preference_set_multiple (SeatonPreference *pGobj,
                                gchar *pTableName,
                                GHashTable* hash,
                                gchar *pConditon)
{

	gchar			*chSqlStr;
	gint 			inResult;
	gint 			inFieldSize;
	gint 			inVal;
 	gchar 	  *pzErrMsg = NULL ;

	GList			*pList;
	GList 		*pTemPtr;

	SeatonPreferencePrivate *priv = pGobj->priv;
	if(pGobj == NULL)
	{
    LIBSEATON_pm_errhandler("ERROR: PDI is NULL");
    return SEATON_PREFERENCE_PM_DBE;
  }

	gchar *pTmpTableName = NULL;
  if(pTableName != NULL)
  {
    pTmpTableName = strdup(pTableName);
  }
  else
  {
     pTmpTableName = strdup("PropertyName");
  }

	inFieldSize = g_hash_table_size(hash);
	pList = g_hash_table_get_keys (hash);
	pTemPtr = pList;

  /*
   * table name is property name
   */
  chSqlStr = g_strdup_printf("UPDATE %s SET ",pTmpTableName);
  gchar *chSqlStrTmp  = NULL;
  for(inVal = 1; inVal <= inFieldSize; inVal++)
  {
    chSqlStrTmp = chSqlStr;
    chSqlStr = g_strconcat(chSqlStrTmp , pTemPtr->data , "=" ,"\'" ,g_hash_table_lookup(hash, pTemPtr->data) , "\'" , "," , NULL);
    g_free(chSqlStrTmp);

    pTemPtr = g_list_nth (pList, inVal);
  }

  chSqlStrTmp = chSqlStr;
  chSqlStr = g_strndup(chSqlStr , (strlen(chSqlStr)-1) ) ;
  g_free(chSqlStrTmp);

  chSqlStrTmp = chSqlStr;
  chSqlStr = g_strconcat(chSqlStrTmp , " WHERE " , pConditon ,  NULL);

  inResult = sqlite3_exec(priv->pDbHandle, chSqlStr, 0, 0, &pzErrMsg);
  g_free(chSqlStr);
  g_free(chSqlStrTmp);
  g_free(pTmpTableName);

  if(inResult != SQLITE_OK)
  {
	  if(NULL != pzErrMsg )
	  {
		  LIBSEATON_pm_errhandler("SQL Error: %s", pzErrMsg);
		  sqlite3_free(pzErrMsg);
	  }
     return SEATON_PREFERENCE_PM_UE;
  }

	return SEATON_PREFERENCE_PM_OK;//success
}

/**
 * seaton_preference_get:
 * @pGobj: The object Parameter
 * @pTableName: The table name
 * @pKeyStr: The  key Name
 * @gCond: the condition for the preference
 *
 * will query the database and extract  the key value pair from persistance database
 * The calling thread is blocked until a reply is received.
 *
 * Returns: an array of persistent key-value pairs
 */
GPtrArray*
seaton_preference_get (SeatonPreference *pGobj,
                       gchar *pTableName,
                       const gchar* pKeyStr,
                       gchar *gCond)
{
	gchar			    *chSqlStr;
	gint 			    iNumCol;
	gint 			    inResult;
	gint 			    inVal;
	sqlite3_stmt  *sqlProg = 0;
	gchar 			  *gColVal;
	gchar 			  *gColName;

	SeatonPreferencePrivate *priv = pGobj->priv;
	if(pGobj == NULL)
	{
    LIBSEATON_pm_errhandler("ERROR: PDI is NULL");
    return NULL;
  }

	gchar *pTmpTableName = NULL;
  if(pTableName != NULL)
  {
    pTmpTableName = strdup(pTableName);
  }
  else
  {
     pTmpTableName = strdup("PropertyName");
  }

	GPtrArray  		*gparray;   //If there is no data in the property name.
	gparray = g_ptr_array_new ();

	GHashTable  *hashchk ;

	if( pKeyStr == NULL || gCond == NULL  )
	{
	   /*
	    * if the condition and key is null
		* the whole table is returned
		*/
		chSqlStr = g_strdup_printf("SELECT * FROM %s;",pTmpTableName);
	}
	else if ( pKeyStr != NULL && gCond != NULL )
	{
	  chSqlStr = g_strdup_printf("SELECT * FROM %s WHERE %s = '%s';",pTmpTableName, pKeyStr, gCond);
	}

	sqlite3_prepare_v2(priv->pDbHandle, chSqlStr, -1, &sqlProg, 0);

	while( (inResult =  sqlite3_step(sqlProg) ) == SQLITE_ROW )
	{
		iNumCol = sqlite3_column_count(sqlProg);
		hashchk =  g_hash_table_new(g_str_hash, g_str_equal);
	  for (inVal = 0; inVal < iNumCol; inVal++)
    {
			 gColVal  = g_strdup((const gchar *)sqlite3_column_text(sqlProg, inVal));
			 gColName = g_strdup((const gchar *)sqlite3_column_name(sqlProg, inVal));
			 g_hash_table_insert(hashchk, gColName, gColVal);
	  }
	  g_ptr_array_add (gparray, ( GHashTable* ) hashchk);
	}
	g_free(pTmpTableName);
	g_free(chSqlStr);
	sqlite3_finalize(sqlProg);

 return gparray;
}

/**
 * seaton_preference_get_multiple:
 * @pGobj: The object Parameter
 * @pTableName: The table name
 * @pWhereFrom: the String from where the value is
 * @gCond: the condition for the preference
 *
 * will query the database and extract  multiple key and value pairs from persistance database *
 * The calling thread is blocked until a reply is received.
 *
 * Returns: an array of key-value pairs
 */
GPtrArray*
seaton_preference_get_multiple (SeatonPreference *pGobj,
                                gchar *pTableName,
                                gchar *pWhereFrom ,
                                gchar *gCond)
{
	char			    *chSqlStr = NULL;
	int 			    iNumCol;
	int 			    inResult;
	int 			    inVal;
	sqlite3_stmt  *sqlProg = 0;
	gchar 			  *gColVal;
	gchar 			  *gColName;

	SeatonPreferencePrivate *priv = pGobj->priv;
	if(pGobj == NULL)
	{
    LIBSEATON_pm_errhandler("ERROR: PDI is NULL");
    return NULL;
  }

	gchar *pTmpTableName = NULL;
  if(pTableName != NULL)
  {
    pTmpTableName = strdup(pTableName);
  }
  else
  {
     pTmpTableName = strdup("PropertyName");
  }

	GPtrArray  		*gparray;
	gparray = g_ptr_array_new ();
	GHashTable  *hashchk ;

	if( pWhereFrom == NULL && gCond == NULL  )
	{
	   /*
	    * if the condition and key is null
		* the whole table is returned
		*/
		chSqlStr = g_strdup_printf("SELECT * FROM %s;",pTmpTableName);
	}
	else if ( pWhereFrom != NULL && gCond != NULL )
	{
		chSqlStr = g_strdup_printf("SELECT %s FROM %s WHERE %s;", pWhereFrom, pTmpTableName, gCond);
	}
	else if ( pWhereFrom != NULL && gCond == NULL )
	{
		chSqlStr = g_strdup_printf("SELECT %s FROM %s;",pWhereFrom,pTmpTableName);
	}
	else if ( pWhereFrom == NULL && gCond != NULL )
	{
		chSqlStr = g_strdup_printf("SELECT * FROM %s WHERE %s;",pTmpTableName,gCond);
	}

	g_assert(chSqlStr != NULL);
	sqlite3_prepare_v2(priv->pDbHandle, chSqlStr, -1, &sqlProg, 0);

	while( (inResult =  sqlite3_step(sqlProg) ) == SQLITE_ROW )
	{
		iNumCol = sqlite3_column_count(sqlProg);
		hashchk =  g_hash_table_new(g_str_hash, g_str_equal);
		for (inVal = 0; inVal < iNumCol; inVal++)
		{
		  gColVal  = g_strdup((const gchar*)sqlite3_column_text(sqlProg, inVal));
      gColName = g_strdup((const gchar*)sqlite3_column_name(sqlProg, inVal));
		  g_hash_table_insert(hashchk, gColName, gColVal);
		}

		 g_ptr_array_add (gparray, ( GHashTable* ) hashchk);
	}
	g_free(pTmpTableName);
	g_free(chSqlStr);
	sqlite3_finalize(sqlProg);

 return gparray;
}


/**
 * seaton_preference_add_data:
 * @pGobj: The object Parameter
 * @pTableName: The table name
 * @hash: The key and  Value will be given
 *
 * function will insert a new key-value pair in the persistence database
 * The calling thread is blocked until a reply is received.
 *
 * Returns: error/success
 */
gint
seaton_preference_add_data (SeatonPreference *pGobj,
                            gchar *pTableName,
                            GHashTable* hash)
{

   gint			inVal;
   gint 			inFieldSize;
   gint 			inResult;
   gchar     *chSqlStr;
   GList			*pList;
   GList 		*pTemPtr;

   sqlite3_stmt    *sqlProg = 0;

   SeatonPreferencePrivate *priv = pGobj->priv;
   if(pGobj == NULL)
   {
      LIBSEATON_pm_errhandler("ERROR: PDI is NULL");
      return SEATON_PREFERENCE_PM_DBE;
   }

   gchar *pTmpTableName = NULL;
   if(pTableName != NULL)
   {
      pTmpTableName = strdup(pTableName);
   }
   else
   {
      pTmpTableName = strdup("PropertyName");
   }

   inFieldSize = g_hash_table_size(hash);

   if(hash == NULL )
   {
      //printf("hash == NULL and %d\n", inFieldSize);
      LIBSEATON_pm_errhandler("ERROR: out of range, hash table is null ");
      return  SEATON_PREFERENCE_PM_OOR;//Out Of Range
   }

   /*
    * extract the list of Key names
    */
   pList = g_hash_table_get_keys (hash);
   pTemPtr = pList;

   /*
    * table name is property name
    */
   chSqlStr = g_strdup_printf("INSERT INTO %s (",pTmpTableName);

   gchar* chSqlStrTmp = NULL;
   for(inVal = 1; inVal <= inFieldSize; inVal++)
   {
      chSqlStrTmp = chSqlStr;
      chSqlStr = g_strconcat(chSqlStrTmp , pTemPtr->data , "," , NULL);
      g_free(chSqlStrTmp);

      pTemPtr = g_list_nth (pList, inVal);
   }

   chSqlStrTmp = chSqlStr;
   chSqlStr = g_strndup(chSqlStr , (strlen(chSqlStr)-1) ) ;
   g_free(chSqlStrTmp);

   chSqlStrTmp = chSqlStr;
   chSqlStr = g_strconcat(chSqlStrTmp ,")" ," VALUES (", NULL);
   g_free(chSqlStrTmp);
   /*  The  loop will get the values which is sent as per the field name
    *  for eg the query will be
    *  INSERT INTO property name(key ) VALUE( data);
    */
   pTemPtr = pList;

   for(inVal = 1; inVal <= inFieldSize; inVal++)
   {
      chSqlStrTmp = chSqlStr;
      //g_print("\n VALUE TO ESCAPE = %s",(gchar *)g_hash_table_lookup(hash, pTemPtr->data));
      gchar *pEscaped =  replace((gchar *)g_hash_table_lookup(hash, pTemPtr->data),"'","''");
      //g_print("\n Escaped Values are = %s \n",pEscaped);
      chSqlStr = g_strconcat(chSqlStrTmp , "\'" , g_strdup(pEscaped) , "\'" , "," , NULL);
      g_free(chSqlStrTmp);
      pTemPtr = g_list_nth (pList, inVal);
   }

   chSqlStrTmp = chSqlStr;
   chSqlStr = g_strndup(chSqlStr , (strlen(chSqlStr)-1) );
   g_free(chSqlStrTmp);

   chSqlStrTmp = chSqlStr;
   chSqlStr = g_strconcat(chSqlStrTmp ,");" , NULL);
 //  g_print( "Constructed SQL Query is = %s \n",chSqlStr );
   inResult = sqlite3_prepare_v2(priv->pDbHandle, chSqlStr, -1, &sqlProg, 0);

   g_free(chSqlStr);
   g_free(chSqlStrTmp);
   g_free(pTmpTableName);

   if(inResult != SQLITE_OK)
   {
      return SEATON_PREFERENCE_PM_UE;
   }

   inResult  = sqlite3_step(sqlProg);
   /*
    *
    */
   inResult  = sqlite3_finalize(sqlProg);

   return SEATON_PREFERENCE_PM_OK;
}

/**
 * replace :
 * @pSource: Source string that has to be changed
 * @pOld: Old string that has to be replaced within the pSource string
 * @pNew: New string that will replace the pOld in the pSource.
 *
 * function will replace the pOld with pNew in the Psource string
 *
 * The calling thread is blocked until a reply is recieve
 *
 * Returns: the replaced string.
 */
static gchar *replace(const gchar *pSource, const gchar *pOld, const gchar *pNew)
{
  gchar *pReturn;
  gint indexLoop;
  gint count = 0;
  size_t newlen = strlen(pNew); //New size for the string
  size_t oldlen = strlen(pOld); //Old Size for the string

  //Get the New size of the string.
  for (indexLoop = 0; pSource[indexLoop] != '\0'; indexLoop++)
  {
    if (strstr(&pSource[indexLoop], pOld) == &pSource[indexLoop]) {
      count++;
      indexLoop += oldlen - 1;
    }
  }

//Allocate the new size for the string.
  //pReturn = malloc(indexLoop + count * (newlen - oldlen));
  pReturn = (gchar *)g_new(gchar *,indexLoop + count * (newlen - oldlen));
  if (pReturn == NULL)
    exit(EXIT_FAILURE);

  indexLoop = 0;
//Replace the pNew with pOld
  while (*pSource)
  {
    if (strstr(pSource, pOld) == pSource) {
      strcpy(&pReturn[indexLoop], pNew);
      indexLoop += newlen;
      pSource += oldlen;
    } else
      pReturn[indexLoop++] = *pSource++;
  }
//Ends with null
  pReturn[indexLoop] = '\0';

  return pReturn;
}

/**
 * seaton_preference_install:
 * @pGobj: The object Parameter
 * @pTableName: The table name
 * @pfieldparam: The field Information
 * @inFieldSize: The field Size
 *
 * will create the Data base Table structure with the name provided. Typically for apps, it is the app name itself.
 * The calling thread is blocked until a reply is received.
 *
 * Returns: error/success
 */
gint
seaton_preference_install (SeatonPreference *pGobj,
                           gchar *pTableName,
                           SeatonFieldParam *pfieldparam,
                           gint inFieldSize)
{

  gchar    *chSqlStr = NULL;
	gint		 inVal;
	gchar 	 *pzErrMsg = NULL;
	gchar 	 *pStrPnt;
	gint 		 inResult;

	SeatonPreferencePrivate *priv = pGobj->priv;
	if(pGobj == NULL)
	{
    LIBSEATON_pm_errhandler("ERROR: PDI is NULL");
    return SEATON_PREFERENCE_PM_DBE;
  }

	gchar *pTmpTableName = NULL;
  if(pTableName != NULL)
  {
    pTmpTableName = strdup(pTableName);
  }
  else
  {
     pTmpTableName = strdup("PropertyName");
  }

	if( pfieldparam == NULL)
	{
		return  SEATON_PREFERENCE_PM_OOR;
	}

  gchar* chSqlStrTmp = NULL;
	if( inFieldSize == 0)
	{
		/*
		 * to use this the table should be created
		 */
    chSqlStr = g_strdup_printf("ALTER TABLE %s ADD COLUMN %s ",pTmpTableName , pfieldparam[0].pKeyName);


		if( pfieldparam[0].inFieldType == SQLITE_INTEGER)
		{
		  chSqlStrTmp = chSqlStr;
			chSqlStr = g_strconcat(chSqlStrTmp ," INT," , NULL);
			g_free(chSqlStrTmp);
		}
		if( pfieldparam[0].inFieldType == SQLITE_TEXT )
		{
		  chSqlStrTmp = chSqlStr;
			chSqlStr = g_strconcat(chSqlStrTmp ," VARCHAR," , NULL);
			g_free(chSqlStrTmp);
		}
		pStrPnt = strrchr(chSqlStr,',');
		strcpy(pStrPnt,";");
	}
	else if (inFieldSize != 0)
	{
    chSqlStr = g_strdup_printf("CREATE TABLE IF NOT EXISTS %s ( ID INTEGER PRIMARY KEY ,",pTmpTableName );

    for (inVal = 0 ; inVal <  inFieldSize; inVal++)
    {
       chSqlStrTmp = chSqlStr;
       chSqlStr = g_strconcat(chSqlStrTmp , pfieldparam[inVal].pKeyName , NULL);
       g_free(chSqlStrTmp);

       if( pfieldparam[inVal].inFieldType == SQLITE_INTEGER)
       {
         chSqlStrTmp = chSqlStr;
         chSqlStr = g_strconcat(chSqlStrTmp , " INT," , NULL);
         g_free(chSqlStrTmp);
       }
       if( pfieldparam[inVal].inFieldType == SQLITE_TEXT )
       {
         chSqlStrTmp = chSqlStr;
         chSqlStr = g_strconcat(chSqlStrTmp , " VARCHAR," , NULL);
         g_free(chSqlStrTmp);
       }
     }

		chSqlStrTmp = chSqlStr;
    chSqlStr = g_strndup(chSqlStr , (strlen(chSqlStr)-1) );
    g_free(chSqlStrTmp);

    chSqlStrTmp = chSqlStr;
    chSqlStr = g_strconcat(chSqlStrTmp ,");" , NULL);
	}

	if( priv->pDbHandle == NULL)
	{
		return SEATON_PREFERENCE_PM_DBE;
	}
	else if(priv->pDbHandle != NULL)
	{
		inResult = sqlite3_exec(priv->pDbHandle, chSqlStr, 0, 0, &pzErrMsg);
    g_free(chSqlStr);

		if(inResult != SQLITE_OK)
    {
      /* An error occurs if the table already exists, but that's 'ok' */
      if(g_strcmp0(pzErrMsg, "table already exists") != 0)
      {
        LIBSEATON_pm_errhandler("SQL error: %s", pzErrMsg);
        if( pzErrMsg )
        {
          sqlite3_free( pzErrMsg );
        }
        sqlite3_close( priv->pDbHandle );
        return SEATON_PREFERENCE_PM_UE;
      }
      sqlite3_free( pzErrMsg );
    }
	}
	return SEATON_PREFERENCE_PM_OK;
}

static gchar *
get_storage_directory (guint        db_type,
                       const gchar *app_name)
{
  /* See https://wiki.apertis.org/Application_Layout */
  switch (db_type)
    {
    case SEATON_PREFERENCE_SERVICE_DB:
      return g_build_filename (G_DIR_SEPARATOR_S, "var", "Applications", app_name,
                               "data", NULL);
    case SEATON_PREFERENCE_APP_INTERNAL_DB:
      /* FIXME: For the moment, until we have proper multi-user support, make
       * the per-bundle and per-(bundle, user) storage paths the same. This is
       * because Ribchester currently doesn’t create the per-bundle directory
       * and its parent directory is owned by root, so the application cannot.
       * It is unclear how the permissions will end up working in this
       * situation. In the mean time, since we do not yet support multi-user
       * systems, all bundles run as the same user anyway, so configuration
       * will not end up being split unnecessarily.
       *
       * https://phabricator.apertis.org/T2197 */
    case SEATON_PREFERENCE_APP_USER_DB:
      return g_strdup (g_get_user_data_dir ());
    default:
      g_assert_not_reached ();
    }
}

/**
 * seaton_preference_open:
 * @pGobj: The object Parameter
 * @pAppName: The unique name from which the Database is created or mapped
 * @pFileName: The unique file for that particular application
 * @uinDBType: per user db or system wide db
 *
 * will open the database in the specified location for reading
 * The calling thread is blocked until a reply is received.
 *
 * Returns: error/success
 */
gint
seaton_preference_open (SeatonPreference *pGobj,
                        const char *pAppName,
                        const char *pFileName,
                        guint uinDBType)
{
	SeatonPreferencePrivate *priv = pGobj->priv;
	sqlite3  	*db;
	gchar* pDBFileName = NULL;

	int   inResult ;

  if( pGobj == NULL )
  {
      return SEATON_PREFERENCE_PM_OOR ; //error
  }

  if( pAppName == NULL )
  {
    return SEATON_PREFERENCE_PM_OOR ; //error
  }

  if( pFileName == NULL )
  {
    pDBFileName = g_strdup(pAppName);
  }
  else
  {
    pDBFileName = g_strdup(pFileName);
  }

  gchar *pFolderPath= NULL;

  pFolderPath = get_storage_directory (uinDBType, pAppName);

  g_mkdir_with_parents(pFolderPath,0755);
  gchar *pFilePath = g_strconcat(pFolderPath,"/",pDBFileName,".db",NULL);
  g_free(pFolderPath);
  pFolderPath = NULL;

  inResult = sqlite3_open_v2(pFilePath, &db, SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE, NULL);
  g_free(pFilePath);
  pFilePath = NULL;

  if( inResult )
  {
    LIBSEATON_pm_errhandler("Error: Cannot open database: %s", sqlite3_errmsg(db));
      sqlite3_close(db);
      return SEATON_PREFERENCE_PM_BDS;
  }
  priv->pDbHandle = db;

  return SEATON_PREFERENCE_PM_OK;
}

/**
 * seaton_preference_check_exists:
 * @pGobj: The object Parameter
 * @pAppName: Application Name
 * @pFileName: File name
 * @uinDBType: Type of the DB (%SEATON_PREFERENCE_SERVICE_DB/%SEATON_PREFERENCE_APP_INTERNAL_DB/%SEATON_PREFERENCE_APP_USER_DB)
 *
 * checks if db existes or not
 * The calling thread is blocked until a reply is received.
 *
 * Returns: error/success
 */
gboolean
seaton_preference_check_exists (SeatonPreference *pGobj,
                                const gchar *pAppName,
                                const char *pFileName,
                                guint uinDBType)
{
  gchar *pFolderPath= NULL;
  gchar* pDBFileName = NULL;

  if( pGobj == NULL )
  {
      return FALSE ; //error
  }

  if( pAppName == NULL )
  {
    return FALSE ; //error
  }

  if( pFileName == NULL )
  {
    pDBFileName = g_strdup(pAppName);
  }
  else
  {
    pDBFileName = g_strdup(pFileName);
  }

  pFolderPath = get_storage_directory (uinDBType, pAppName);

  gchar *pFilePath = g_strconcat(pFolderPath,"/",pDBFileName,".db",NULL);

  g_free(pFolderPath);
  pFolderPath = NULL;

  GFile *pFile = g_file_new_for_commandline_arg (pFilePath);
  if(pFile != NULL)
  {
	if(g_file_query_exists(pFile,NULL))
        {
		g_print("\n File Exists \n");
  		return TRUE;
	}
	else
	{
		g_print("\n File doesn't Exists \n");
  		return FALSE;
	}
	return FALSE;
  }
	return FALSE;
}

/**
 * seaton_preference_close:
 * @pGobj: The object Parameter
 *
 * will close the database handle. No further operations are possible.
 * <note>
 * 	This API should be called atleast in the GObject dispose function to ensure database integrity
 * </note>
 *
 * The calling thread is blocked until a reply is received.
 *
 * Returns: error/success
 */
gint
seaton_preference_close (SeatonPreference *pGobj)
{
	int inResult;
	SeatonPreferencePrivate *priv = pGobj->priv;
	if( pGobj == NULL )
	{
	  	return SEATON_PREFERENCE_PM_OOR ; //error
	}

	inResult = sqlite3_close( priv->pDbHandle );
	if(inResult != SQLITE_OK)
	{
		LIBSEATON_pm_errhandler("SQL Error: %s", sqlite3_errmsg(priv->pDbHandle));
	    return SEATON_PREFERENCE_PM_UE;
	}
  /*
  *
  */
	return SEATON_PREFERENCE_PM_OK;
}


